$(window).ready(function () {
    (function () {
        var $rows = $('#tracker tbody tr').not('#today');

        $('#show-hide').on('click', function () {
            $rows.toggle();
        });

        $rows.hide();
    })();

    (function () {
        $('#tracker').on('change', '.start-time, .stop-time', function () {
            var $parent = $(this).parent();
            var $cell = $parent.parent();
            var $startTime = $parent.find('.start-time').first();
            var $stopTime = $parent.find('.stop-time').first();

            var formData = new FormData();
            var xhr = new XMLHttpRequest();

            formData.append('user', $cell.data('user'));
            formData.append('date', $cell.data('date'));
            formData.append('time', $parent.data('time'));
            formData.append('start_time', $startTime.val());
            formData.append('stop_time', $stopTime.val());

            xhr.addEventListener('readystatechange', function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        var response = JSON.parse(this.responseText);

                        $startTime.val(response.start_time);
                        $stopTime.val(response.stop_time);
                        $parent.data('time', response.id);

                        if (response.last && $parent.next().length === 0) {
                            $cell.append(`
                                <div class="input-group input-group-sm mt-sm-1">
                                    <input type="text" class="form-control start-time">
                                    <div class="input-group-prepend input-group-sm">
                                        <span class="input-group-text">-</span>
                                    </div>
                                    <input type="text" class="form-control stop-time">
                                </div>
                            `);
                        }
                    } else if (this.status === 422) {
                        var errors = JSON.parse(this.responseText);
                        alert(errors.join('\n'));
                    } else {
                        alert('Server error');
                    }
                }
            });

            xhr.open('POST', '/admin/index/save');
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(formData);
        });
    })();
});
