$(window).ready(function () {
    (function () {
        var $rows = $('#tracker tbody tr').not('#today');

        $('#show-hide').on('click', function () {
            $rows.toggle();
        });

        $rows.hide();
    })();

    (function () {
        $('#tracker').on('click', '#start-time', function () {
            var $that = $(this);
            var $parent = $that.parent();
            var xhr = new XMLHttpRequest();

            $that.text('...');

            xhr.addEventListener('readystatechange', function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        var response = JSON.parse(this.responseText);

                        $that.replaceWith(response.time);

                        if (response.late) {
                            $parent.parents('td').css('background-color', '#ffb9b2');
                        }

                        if (response.reload) {
                            window.location.reload();
                        } else {
                            $parent.find('span').replaceWith(`
                                <span class="btn btn-sm btn-danger" id="stop-time">Stop</span>
                            `);
                        }
                    } else {
                        $that.replaceWith('ERROR');
                    }
                }
            });

            xhr.open('POST', '/index/start?day=' + $parent.parents('.cell').data('day'));
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();
        });

        $('#tracker').on('click', '#stop-time', function () {
            var $that = $(this);
            var $cell = $that.parents('.cell');
            var xhr = new XMLHttpRequest();

            $that.text('...');

            xhr.addEventListener('readystatechange', function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        var response = JSON.parse(this.responseText);

                        $that.replaceWith(response.time);

                        if (response.reload) {
                            window.location.reload();
                        } else {
                            $cell.append(`
                                <div class="text-nowrap">
                                    <span class="btn btn-sm btn-primary" id="start-time">Start</span> -
                                    <span>...</span>
                                </div>
                            `);
                        }
                    } else {
                        $that.replaceWith('ERROR');
                    }
                }
            });

            xhr.open('POST', '/index/stop?day=' + $cell.data('day'));
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();
        })
    })();
});
