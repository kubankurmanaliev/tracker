<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'App\Models'      => $config->application->modelsDir,
    'App\Forms'       => $config->application->formsDir,
    'App\Controllers' => $config->application->controllersDir,
    'App\Plugins'     => $config->application->pluginsDir,
    'App'             => $config->application->libraryDir
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';

date_default_timezone_set('Asia/Bishkek');
