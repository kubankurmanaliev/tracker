<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group;

/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Router();

$router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);

$router->add('/', [
    'controller' => 'index',
    'action'     => 'index'
]);

$router->add('/logout', [
    'controller' => 'login',
    'action'     => 'logout'
]);

// Admin
$admin = new Group([
    'namespace' => 'App\Controllers\Admin',
]);

$admin->setPrefix('/admin');

$admin->add('[/]{0,1}', [
    'controller' => 'index',
    'action'     => 'index'
]);

$admin->add('/:controller[/]{0,1}', [
    'controller' => 1
]);

$admin->add('/:controller/:action[/]{0,1}', [
    'controller' => 1,
    'action'     => 2
]);

$admin->add('/:controller/:action/:params[/]{0,1}', [
    'controller' => 1,
    'action'     => 2,
    'params'     => 3
]);

$router->mount($admin);

return $router;
