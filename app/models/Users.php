<?php

namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $role;

    /**
     *
     * @var string
     */
    public $login;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $deleted;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'App\Models\Tracker', 'user_id', ['alias' => 'trackedTimes', 'order' => 'start_time']);
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add('login', new Uniqueness([
            'message' => 'The login is already registered'
        ]));

        $validator->add('email', new Uniqueness([
            'message' => 'The email is already registered'
        ]));

        return $this->validate($validator);
    }

    public function setPassword($value)
    {
        if ($value) $this->password = $this->getDI()->get('security')->hash($value);
    }
}
