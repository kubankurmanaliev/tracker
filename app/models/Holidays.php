<?php

namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Holidays extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var string
     */
    public $repeat;

    public function validation()
    {
        $validator = new Validation();

        $validator->add('date', new Uniqueness([
            'message' => 'The date is already exists'
        ]));

        return $this->validate($validator);
    }
}
