<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class Latenesses extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $time_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('time_id', 'App\Models\Tracker', 'id', ['alias' => 'time']);
        $this->belongsTo('user_id', 'App\Models\Users', 'id', ['alias' => 'user']);
    }
}
