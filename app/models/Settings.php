<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class Settings extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $key;

    /**
     *
     * @var string
     */
    public $value;

    public static function get($key, $default = null)
    {
        $setting = static::findFirst(['key' => $key]);

        if ($setting && $setting->value)
            return $setting->value;

        return $default;
    }
}
