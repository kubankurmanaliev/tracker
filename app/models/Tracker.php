<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class Tracker extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $start_time;

    /**
     *
     * @var string
     */
    public $stop_time;

    /**
     *
     * @var integer
     */
    public $total;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('user_id', 'App\Models\Users', 'id', ['alias' => 'user']);
    }
}
