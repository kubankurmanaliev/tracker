<?php

namespace App\Forms\Admin;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class HolidaysForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $name = new Text('name', [
            'placeholder' => 'Name'
        ]);

        $name->addValidators([
            new StringLength([
                'max'            => 255,
                'messageMaximum' => 'The name max length 255'
            ])
        ]);

        $this->add($name);

        $date = new Date('date', [
            'placeholder' => 'Date'
        ]);

        $date->addValidators([
            new PresenceOf([
                'message' => 'The date is required'
            ])
        ]);

        $this->add($date);

        $repeat = new Check('repeat', [
            'value' => 'Y'
        ]);

        $this->add($repeat);
    }
}
