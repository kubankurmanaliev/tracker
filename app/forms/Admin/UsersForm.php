<?php

namespace App\Forms\Admin;

use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class UsersForm extends Form
{
    public function initialize($entity = null, $options = null)
    {

        $name = new Text('name', [
            'placeholder' => 'Name'
        ]);

        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ]),
            new StringLength([
                'max'            => 255,
                'messageMaximum' => 'The name max length 255'
            ])
        ]);

        $this->add($name);

        $email = new Text('email', [
            'placeholder' => 'Email'
        ]);

        $email->addValidators([
            new PresenceOf([
                'message' => 'The e-mail is required'
            ]),
            new StringLength([
                'max'            => 255,
                'messageMaximum' => 'The e-mail max length 255'
            ]),
            new Email([
                'message' => 'The e-mail is not valid'
            ])
        ]);

        $this->add($email);

        $login = new Text('login', [
            'placeholder' => 'Login'
        ]);

        $login->addValidators([
            new PresenceOf([
                'message' => 'The login is required'
            ]),
            new StringLength([
                'min'            => 6,
                'max'            => 255,
                'messageMinimum' => 'The login min length 6',
                'messageMaximum' => 'The login max length 255'
            ])
        ]);

        $this->add($login);

        $password = new Password('password', [
            'placeholder' => 'Password'
        ]);

        if ($this->dispatcher->getActionName() == 'create') {
            $password->addValidator(new PresenceOf([
                'message' => 'The password is required'
            ]));
        }

        if ($this->request->getPost('password')) {
            $password->addValidator(new StringLength([
                'min'            => 6,
                'max'            => 255,
                'messageMinimum' => 'The password min length 6',
                'messageMaximum' => 'The password max length 255'
            ]));
        }

        $this->add($password);

        $this->add(new Select('role', [
            'user'  => 'User',
            'admin' => 'Administrator'
        ]));
    }
}
