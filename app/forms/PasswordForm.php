<?php

namespace App\Forms;

use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class PasswordForm extends Form
{
    public function initialize()
    {
        $password = new Password('password', [
            'placeholder' => 'Change password'
        ]);

        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min'            => 6,
                'max'            => 255,
                'messageMinimum' => 'The password min length 6',
                'messageMaximum' => 'The password max length 255'
            ])
        ]);

        $password->setLabel('Change password');
        $password->clear();

        $this->add($password);

        $confirm = new Password('password_confirmation', [
            'placeholder' => 'Confirm password'
        ]);

        $confirm->addValidator(new Confirmation([
            'message' => 'Password doesn\'t match confirmation',
            'with'    => 'password'
        ]));

        $confirm->setLabel('Confirm password');
        $confirm->clear();

        $this->add($confirm);
    }
}
