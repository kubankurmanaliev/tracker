<?php

namespace App\Controllers;

use App\Forms\LoginForm;

class LoginController extends ControllerBase
{
    public function indexAction()
    {
        $form = new LoginForm();

        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    $this->auth->check([
                        'login'    => $this->request->getPost('login'),
                        'password' => $this->request->getPost('password'),
                    ]);

                    $user = $this->auth->getUser();

                    if ($user->role === 'admin')
                        return $this->response->redirect('/admin/index');

                    return $this->response->redirect('/');
                }
            }
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->form = $form;
        $this->view->setTemplateBefore('empty');
    }

    public function logoutAction()
    {
        $this->auth->remove();

        return $this->response->redirect('/login');
    }
}
