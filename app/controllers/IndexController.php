<?php

namespace App\Controllers;

use App\Models\Holidays;
use App\Models\Latenesses;
use App\Models\Settings;
use App\Models\Tracker;
use App\Models\Users;
use Carbon\Carbon;

class IndexController extends ControllerBase
{
    const MONTHS = [
        1  => 'January',
        2  => 'February',
        3  => 'March',
        4  => 'April',
        5  => 'May',
        6  => 'June',
        7  => 'July',
        8  => 'August',
        9  => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    ];

    const YEAR_FROM = 2009;

    public function indexAction()
    {
        $this->view->months = static::MONTHS;
        $this->view->yearFrom = static::YEAR_FROM;
        $this->view->currentYear = $this->getCurrentYear();
        $this->view->currentMonth = $this->getCurrentMonth();
        $this->view->currentDate = Carbon::createFromDate($this->view->currentYear, $this->view->currentMonth, 1);
        $this->view->holidays = $this->getHolidays($this->view->currentDate);
        $this->view->users = $this->getUsersWithTimes($this->view->currentDate);
        $this->view->totals = $this->getTotals($this->view->users[0], clone $this->view->currentDate,
            $this->view->holidays);

        $this->assets->addJs('js/tracker.js');
    }

    public function startAction()
    {
        $this->view->disable();

        if (!$this->request->isPost() || !$this->request->isAjax())
            return $this->methodNotAllowed();

        $user = $this->auth->getUser();

        if (!$user)
            return $this->forbidden();

        $timestamp = new Carbon();
        $times = Tracker::find([
            'conditions' => 'user_id = :user_id: AND DATE(start_time) = :date:',
            'bind'       => [
                'user_id' => $user->id,
                'date'    => $timestamp->format('Y-m-d')
            ],
            'order'      => 'start_time'
        ]);
        $last = $times->getLast();

        if ($last && !$last->stop_time)
            return $this->forbidden();

        $last = new Tracker();
        $last->user_id = $user->id;
        $last->start_time = $timestamp->format('Y-m-d H:i:s');
        $last->create();

        $response = ['time' => $timestamp->format('H:i')];
        $workStartTime = Carbon::createFromTimeString(Settings::get('work_start_time', '09:00'));

        if (!count($times) && ($workStartTime->getTimestamp() - $timestamp->getTimestamp()) < 0) {
            $late = new Latenesses();
            $late->time_id = $last->id;
            $late->user_id = $user->id;
            $late->save();

            $response['late'] = true;
        }

        if ($this->request->get('day', 'int') != $timestamp->day)
            $response['reload'] = true;

        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($response));

        return $this->response;
    }

    public function stopAction()
    {
        $this->view->disable();

        if (!$this->request->isPost() || !$this->request->isAjax())
            return $this->methodNotAllowed();

        $user = $this->auth->getUser();

        if (!$user)
            return $this->forbidden();

        $timestamp = new Carbon();
        $last = Tracker::findFirst([
            'conditions' => 'user_id = :user_id: AND DATE(start_time) <= :date:',
            'bind'       => [
                'user_id' => $user->id,
                'date'    => $timestamp->format('Y-m-d')
            ],
            'order'      => 'start_time DESC'
        ]);

        if ($last && $last->stop_time)
            return $this->forbidden();

        $last->stop_time = $timestamp->format('Y-m-d H:i:s');
        $last->save();

        $response = ['time' => $timestamp->format('H:i')];

        if ($this->request->get('day', 'int') != $timestamp->day)
            $response['reload'] = true;

        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($response));

        return $this->response;
    }

    protected function getUsersWithTimes($date)
    {
        $currentUser = $this->auth->getUser()->toArray();
        $users = array_merge([$currentUser], Users::find([
            'conditions' => 'deleted = :deleted: AND id != :id:',
            'bind'       => ['deleted' => 'N', 'id' => $currentUser['id']],
            'order'      => 'name'
        ])->toArray());
        $times = $this->getTimesByUsersAndMonth(array_column($users, 'id'), $date);

        foreach ($users as &$user) {
            $tmpTimes = [];
            $userTimes = array_filter($times, function ($value) use ($user) {
                return $value['user_id'] === $user['id'];
            });

            foreach ($userTimes as $time) {
                $tmpTimes[Carbon::createFromTimeString($time['start_time'])->day][] = (object)$time;
            }

            $user = (object)array_merge($user, [
                'current'      => $user['id'] === $currentUser['id'],
                'trackedTimes' => $tmpTimes
            ]);
        }

        return $users;
    }

    protected function getTimesByUsersAndMonth($usersIds, $date)
    {
        return Tracker::query()
            ->where('YEAR(start_time) = :year: AND MONTH(start_time) = :month:', [
                'year'  => $date->year,
                'month' => $date->month
            ])
            ->inWhere('user_id', $usersIds)
            ->orderBy('start_time')
            ->execute()
            ->toArray();
    }

    protected function getHolidays($date)
    {
        $holidays = Holidays::query()
            ->where('(repeat = "N" AND YEAR(date) = :year: AND MONTH(date) = :month:) OR (repeat = "Y" AND MONTH(date) = :month:)')
            ->bind([
                'year'  => $date->year,
                'month' => $date->month
            ])
            ->orderBy('FIELD(repeat, "Y", "N"), DAY(date)')
            ->execute();
        $prepared = [];

        foreach ($holidays as $holiday) {
            $prepared[Carbon::createFromFormat('Y-m-d', $holiday->date)->day] = $holiday->name;
        }

        return $prepared;
    }

    protected function getCurrentYear()
    {
        $current = date('Y');
        $request = $this->request->get('year', 'int');

        if ($request >= static::YEAR_FROM && $request <= $current)
            return $request;

        return $current;
    }

    protected function getCurrentMonth()
    {
        $current = date('m');
        $request = $this->request->get('month', 'int');

        if (in_array($request, array_keys(static::MONTHS)))
            return $request;

        return $current;
    }

    protected function getTotals($user, $date, $holidays)
    {
        $assigned = $this->getWorkHours(clone $date, $holidays);
        $have = $this->getWorkedTime($user);

        return [
            'assigned'   => $assigned,
            'have'       => Carbon::createFromTimestampUTC($have)->format('H:i'),
            'percent'    => round((($assigned) * ($have / 3600) / 100), 2),
            'work_start' => Settings::get('work_start_time', '09:00'),
            'lates'      => count($this->modelsManager->executeQuery(
                'SELECT * FROM App\Models\Latenesses l INNER JOIN App\Models\Tracker t ON l.time_id = t.id WHERE YEAR(t.start_time) = :year: AND MONTH(t.start_time) = :month:',
                [
                    'year'  => $date->year,
                    'month' => $date->month
                ]
            ))
        ];
    }

    protected function getWorkHours($date, $holidays)
    {
        $month = $date->month;
        $days = 0;

        while ($date->month == $month) {
            if (!$date->isWeekend() && !in_array($date->day, array_keys($holidays)))
                $days++;

            $date->addDay();
        }

        return $days * 8;
    }

    protected function getWorkedTime($user)
    {
        $seconds = 0;
        $timestamp = time();

        foreach ($user->trackedTimes as $dayTimes) {
            foreach ($dayTimes as $day => $time) {
                $start = strtotime($time->start_time);
                $stop = $time->stop_time ? strtotime($time->stop_time) : $timestamp;
                $seconds += $stop - $start;
            }
        }

        return $seconds;
    }
}
