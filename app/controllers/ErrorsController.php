<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;

class ErrorsController extends Controller
{
    /**
     * Initialize.
     *
     * @return void
     */
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        $this->view->setTemplateBefore('empty');
    }

    public function error401Action()
    {
    }

    public function error403Action()
    {
    }

    public function error404Action()
    {
    }

    public function error405Action()
    {
    }
}
