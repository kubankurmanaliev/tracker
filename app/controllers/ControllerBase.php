<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;

abstract class ControllerBase extends Controller
{
    protected function forbidden()
    {
        $this->response->setStatusCode(403);
        $this->dispatcher->forward([
            'controller' => 'errors',
            'action'     => 'error403'
        ]);
    }

    protected function methodNotAllowed()
    {
        $this->response->setStatusCode(405);
        $this->dispatcher->forward([
            'controller' => 'errors',
            'action'     => 'error405'
        ]);
    }
}
