<?php

namespace App\Controllers;

use App\Forms\PasswordForm;

class PasswordController extends ControllerBase
{
    public function indexAction()
    {
        $form = new PasswordForm();
        $form->setUserOption('action', '/password');

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) === false) {
                foreach ($form->getMessages() as $message)
                    $this->flash->error($message);
            } else {
                try {
                    $user = $this->auth->getUser();
                } catch (\Exception $e) {
                    if ($e->getCode() === 401)
                        return $this->response->redirect('/login');

                    throw new \Exception($e->getMessage());
                }

                $this->flash->success('Password changed!');
                $user->update(['password' => $this->request->getPost('password')]);
            }
        }

        $this->view->setTemplateBefore('index');
        $this->view->form = $form;
    }
}
