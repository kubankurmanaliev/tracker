<?php

namespace App\Controllers\Admin;

use App\Forms\Admin\UsersForm;
use App\Models\Users;
use Phalcon\Paginator\Adapter\Model as Paginator;

class UsersController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Users');
        $this->view->routePrefix = '/admin/users/';
    }

    public function indexAction()
    {
        $paginator = new Paginator([
            'data'  => Users::query()->where('deleted = :deleted:', ['deleted' => 'N'])->orderBy('name')->execute(),
            'limit' => $this->perPage,
            'page'  => $this->request->get('page', 'int') ?: 1
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    public function createAction()
    {
        $form = new UsersForm();
        $form->setUserOption('action', '/admin/users/create');

        if ($this->request->isPost()) {
            $entity = new Users();
            $data = $this->request->getPost();

            if ($form->isValid($data, $entity)) {
                if ($entity->save()) {
                    $this->flashSession->success('Successfully created!');

                    return $this->response->redirect('/admin/users/update/' . $entity->id);
                } else {
                    foreach ($entity->getMessages() as $message)
                        $this->flash->error($message);
                }
            } else {
                foreach ($form->getMessages() as $message)
                    $this->flash->error($message);
            }
        }

        $this->view->form = $form;
        $this->view->pick('users/form');
    }

    public function updateAction($id)
    {
        $entity = Users::findFirstById($id);
        if (!$entity) return $this->pageNotFound();

        $form = new UsersForm($entity);
        $form->setUserOption('action', '/admin/users/update/' . $entity->id);

        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            if ($form->isValid($data, $entity)) {
                if ($entity->update()) {
                    $this->flashSession->success('Successfully updated!');

                    return $this->response->redirect('/admin/users/update/' . $entity->id);
                } else {
                    foreach ($entity->getMessages() as $message)
                        $this->flash->error($message);
                }
            } else {
                foreach ($form->getMessages() as $message)
                    $this->flash->error($message);
            }
        }

        $this->view->form = $form;
        $this->view->pick('users/form');
    }

    public function destroyAction($id)
    {
        if (!$this->request->isPost())
            $this->methodNotAllowed();

        $entity = Users::findFirstById($id);
        if (!$entity) return $this->pageNotFound();

        $entity->update(['deleted' => 'Y']);
        $this->flashSession->success('Successfully deleted!');

        return $this->response->redirect('/admin/users');
    }
}
