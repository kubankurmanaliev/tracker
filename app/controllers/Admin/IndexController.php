<?php

namespace App\Controllers\Admin;

use App\Controllers\IndexController as IC;
use App\Models\Holidays;
use App\Models\Tracker;
use App\Models\Users;
use Carbon\Carbon;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->months = IC::MONTHS;
        $this->view->yearFrom = IC::YEAR_FROM;
        $this->view->currentYear = $this->getCurrentYear();
        $this->view->currentMonth = $this->getCurrentMonth();
        $this->view->currentDate = Carbon::createFromDate($this->view->currentYear, $this->view->currentMonth, 1);
        $this->view->holidays = $this->getHolidays($this->view->currentDate);
        $this->view->users = $this->getUsersWithTimes($this->view->currentDate);

        $this->assets->addJs('admin/js/tracker.js');
    }

    public function saveAction()
    {
        $messages = $this->validate($this->request->getPost());
        $errors = [];

        if (count($messages)) {
            foreach ($messages as $message)
                $errors[] = $message->getMessage();

            $this->response->setContentType('application/json', 'UTF-8');
            $this->response->setContent(json_encode($errors));
            $this->response->setStatusCode(422);

            return $this->response;
        }

        $user = Users::findFirstById($this->request->getPost('user', 'int'));
        $date = $this->request->getPost('date');
        $times = Tracker::find([
            'conditions' => 'user_id = :user_id: AND DATE(start_time) = :date:',
            'bind'       => ['user_id' => $user->id, 'date' => $date],
            'order'      => 'start_time'
        ]);

        if (($time = $this->request->getPost('time', 'int')))
            $time = Tracker::findFirstById($time);
        else
            $time = new Tracker();

        $response = [];

        $start = Carbon::createFromFormat('Y-m-d', $date)->setTimeFromTimeString($this->request->getPost('start_time'));
        $time->start_time = $start->format('Y-m-d H:i:s');
        $response['start_time'] = $start->format('H:i');

        if (($stopTime = $this->request->getPost('stop_time'))) {
            $stop = Carbon::createFromFormat('Y-m-d', $date)->setTimeFromTimeString($stopTime);
            $time->stop_time = $stop->format('Y-m-d H:i:s');
            $response['stop_time'] = $stop->format('H:i');
        } else {
            $time->stop_time = null;
        }

        $time->user_id = $user->id;
        $time->save();
        $response['id'] = $time->id;

        if (count($times) <= 1 || $time->id >= $times->getLast()->id)
            $response['last'] = true;

        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($response));

        return $this->response;
    }

    protected function validate($data)
    {
        $validator = new Validation();

        // $validator->add('user', new PresenceOf(['message' => 'The user is required']));
        // $validator->add('date', new PresenceOf(['message' => 'The date is required']));
        // $validator->add('date', new Date(['message' => 'The date must be a valid date (YYYY-MM-DD)']));
        $validator->add('start_time', new PresenceOf(['message' => 'The start time is required']));
        $validator->add('start_time', new Regex([
            'pattern' => '/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/',
            'message' => 'The start time must be a valid time'
        ]));

        if (isset($data['stop_time']) && $data['stop_time'])
            $validator->add('stop_time', new Regex([
                'pattern' => '/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/',
                'message' => 'The stop time must be a valid time (HH:MM)'
            ]));

        return $validator->validate($data);
    }

    protected function getUsersWithTimes($date)
    {
        $users = Users::find([
            'conditions' => 'deleted = :deleted:',
            'bind'       => ['deleted' => 'N'],
            'order'      => 'name'
        ])->toArray();
        $times = $this->getTimesByUsersAndMonth(array_column($users, 'id'), $date);

        foreach ($users as &$user) {
            $tmpTimes = [];
            $userTimes = array_filter($times, function ($value) use ($user) {
                return $value['user_id'] === $user['id'];
            });

            foreach ($userTimes as $time) {
                $tmpTimes[Carbon::createFromTimeString($time['start_time'])->day][] = (object)$time;
            }

            $user = (object)array_merge($user, ['trackedTimes' => $tmpTimes]);
        }

        return $users;
    }

    protected function getTimesByUsersAndMonth($usersIds, $date)
    {
        return Tracker::query()
            ->where('YEAR(start_time) = :year: AND MONTH(start_time) = :month:', [
                'year'  => $date->year,
                'month' => $date->month
            ])
            ->inWhere('user_id', $usersIds)
            ->orderBy('start_time')
            ->execute()
            ->toArray();
    }

    protected function getHolidays($date)
    {
        $holidays = Holidays::query()
            ->where('(repeat = "N" AND YEAR(date) = :year: AND MONTH(date) = :month:) OR (repeat = "Y" AND MONTH(date) = :month:)')
            ->bind([
                'year'  => $date->year,
                'month' => $date->month
            ])
            ->orderBy('FIELD(repeat, "Y", "N"), DAY(date)')
            ->execute();
        $prepared = [];

        foreach ($holidays as $holiday) {
            $prepared[Carbon::createFromFormat('Y-m-d', $holiday->date)->day] = $holiday->name;
        }

        return $prepared;
    }

    protected function getCurrentYear()
    {
        $current = date('Y');
        $request = $this->request->get('year', 'int');

        if ($request >= IC::YEAR_FROM && $request <= $current)
            return $request;

        return $current;
    }

    protected function getCurrentMonth()
    {
        $current = date('m');
        $request = $this->request->get('month', 'int');

        if (in_array($request, array_keys(IC::MONTHS)))
            return $request;

        return $current;
    }
}
