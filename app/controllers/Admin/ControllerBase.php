<?php

namespace App\Controllers\Admin;

use Phalcon\Mvc\Controller;

abstract class ControllerBase extends Controller
{
    protected $perPage = 25;

    public function initialize()
    {
        $this->view->setViewsDir(APP_PATH . '/views/admin/');
        $this->view->setLayout('index');
    }

    protected function pageNotFound()
    {
        $this->response->setStatusCode(404);
        $this->view->setViewsDir(APP_PATH . '/views/');
        $this->view->setLayout('');
        $this->dispatcher->forward([
            'namespace'  => 'App\Controllers',
            'controller' => 'errors',
            'action'     => 'error404'
        ]);
    }

    protected function methodNotAllowed()
    {
        $this->response->setStatusCode(405);
        $this->view->setViewsDir(APP_PATH . '/views/');
        $this->view->setLayout('');
        $this->dispatcher->forward([
            'namespace'  => 'App\Controllers',
            'controller' => 'errors',
            'action'     => 'error405'
        ]);
    }
}
