<?php

namespace App\Controllers\Admin;

use App\Models\Settings;

class SettingsController extends ControllerBase
{
    public function saveAction()
    {
        if (!$this->request->isPost())
            $this->methodNotAllowed();

        $settings = $this->request->getPost('settings');

        if (!is_array($settings))
            $settings = [];

        foreach ($settings as $key => $value) {
            $setting = Settings::findFirst(['key' => $key]);

            if (!$setting)
                $setting = new Settings();

            $setting->key = $key;
            $setting->value = $value;
            $setting->save();
        }

        return $this->response->redirect($_SERVER['HTTP_REFERER']);
    }
}
