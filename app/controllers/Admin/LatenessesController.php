<?php

namespace App\Controllers\Admin;

use App\Models\Latenesses;
use App\Models\Settings;
use Phalcon\Paginator\Adapter\Model as Paginator;

class LatenessesController extends ControllerBase
{
    protected $perPage = 100;

    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Latenesses');
        $this->view->routePrefix = '/admin/latenesses/';
    }

    public function indexAction()
    {
        $paginator = new Paginator([
            'data'  => Latenesses::query()->orderBy('id DESC')->execute(),
            'limit' => $this->perPage,
            'page'  => $this->request->get('page', 'int') ?: 1
        ]);

        $this->view->page = $paginator->getPaginate();
        $this->view->workStartTime = Settings::get('work_start_time');
    }

    public function destroyAction($id)
    {
        if (!$this->request->isPost())
            $this->methodNotAllowed();

        $entity = Latenesses::findFirstById($id);
        if (!$entity) return $this->pageNotFound();

        $entity->delete();
        $this->flashSession->success('Successfully deleted!');

        return $this->response->redirect('/admin/latenesses');
    }
}
