<?php

namespace App\Controllers\Admin;

use App\Forms\Admin\HolidaysForm;
use App\Models\Holidays;
use Phalcon\Paginator\Adapter\Model as Paginator;

class HolidaysController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Holidays');
        $this->view->routePrefix = '/admin/holidays/';
    }

    public function indexAction()
    {
        $paginator = new Paginator([
            'data'  => Holidays::query()->orderBy('FIELD(repeat, "Y", "N"), date DESC')->execute(),
            'limit' => $this->perPage,
            'page'  => $this->request->get('page', 'int') ?: 1
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    public function createAction()
    {
        $form = new HolidaysForm();
        $form->setUserOption('action', '/admin/holidays/create');

        if ($this->request->isPost()) {
            $entity = new Holidays();
            $data = $this->request->getPost();

            if ($form->isValid($data, $entity)) {
                if ($entity->save()) {
                    $this->flashSession->success('Successfully created!');

                    return $this->response->redirect('/admin/holidays/update/' . $entity->id);
                } else {
                    foreach ($entity->getMessages() as $message)
                        $this->flash->error($message);
                }
            } else {
                foreach ($form->getMessages() as $message)
                    $this->flash->error($message);
            }
        }

        $this->view->form = $form;
        $this->view->pick('holidays/form');
    }

    public function updateAction($id)
    {
        $entity = Holidays::findFirstById($id);
        if (!$entity) return $this->pageNotFound();

        $form = new HolidaysForm($entity);
        $form->setUserOption('action', '/admin/holidays/update/' . $entity->id);

        if ($this->request->isPost()) {
            $data = $this->request->getPost();

            if ($form->isValid($data, $entity)) {
                if ($entity->update()) {
                    $this->flashSession->success('Successfully updated!');

                    return $this->response->redirect('/admin/holidays/update/' . $entity->id);
                } else {
                    foreach ($entity->getMessages() as $message)
                        $this->flash->error($message);
                }
            } else {
                foreach ($form->getMessages() as $message)
                    $this->flash->error($message);
            }
        }

        $this->view->form = $form;
        $this->view->pick('holidays/form');
    }

    public function destroyAction($id)
    {
        if (!$this->request->isPost())
            $this->methodNotAllowed();

        $entity = Holidays::findFirstById($id);
        if (!$entity) return $this->pageNotFound();

        $entity->delete();
        $this->flashSession->success('Successfully deleted!');

        return $this->response->redirect('/admin/holidays');
    }
}
