<?php

namespace App\Plugins;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;

class ErrorsPlugin extends Plugin
{
    public function beforeException($event, $dispatcher, $exception)
    {
        switch ($exception->getCode()) {
            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                $dispatcher->forward([
                    'namespace'  => 'App\Controllers',
                    'controller' => 'errors',
                    'action'     => 'error404',
                ]);

                return false;
            default:
                return true;
        }
    }
}
