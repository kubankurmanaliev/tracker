<?php

namespace App\Plugins;

use Phalcon\Mvc\User\Plugin;

class SecurityPlugin extends Plugin
{
    public function beforeExecuteRoute($event, $dispatcher)
    {
        $role = 'guest';
        $resource = $dispatcher->getControllerClass();

        try {
            $user = $this->auth->getUser();
        } catch (\Exception $e) {
            if ($e->getCode() === 401)
                return $this->response->redirect('/login');

            throw new \Exception($e->getMessage());
        }

        if ($user) {
            $role = $user->role;
        }

        if (!$this->acl->isResource($resource)) {
            switch ($dispatcher->getNamespaceName()) {
                case 'App\Controllers\Admin':
                    $resource = 'admin';
                    break;
                case 'App\Controllers':
                    $resource = 'user';
                    break;
            }
        }

        if (!$this->acl->isAllowed($role, $resource, $dispatcher->getActionName())) {
            if ($role !== 'guest') {
                return $dispatcher->forward([
                    'namespace'  => 'App\Controllers',
                    'controller' => 'errors',
                    'action'     => 'error401'
                ]);
            }

            return $this->response->redirect('/login');
        }

        return true;
    }
}
