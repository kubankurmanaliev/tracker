<?php

namespace App\Auth;

use App\Models\Users;
use Phalcon\Mvc\User\Component;

class Auth extends Component
{
    public function check($credentials)
    {
        // Check if the user exist
        $user = Users::findFirstByLogin($credentials['login']);

        if ($user == false) {
            throw new \Exception('Wrong login/password combination');
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            throw new \Exception('Wrong login/password combination');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id'   => $user->id,
            'name' => $user->name,
            'role' => $user->role
        ]);
    }

    public function checkUserFlags(Users $user)
    {
        if ($user->deleted === 'Y') {
            throw new \Exception('The user is deleted');
        }
    }

    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    public function getName()
    {
        $identity = $this->session->get('auth-identity');

        return $identity['name'];
    }

    public function remove()
    {
        $this->session->remove('auth-identity');
    }

    public function authUserById($id)
    {
        $user = Users::findFirst([
            'conditions' => 'id = :id: AND deleted = :deleted:',
            'bind'       => [
                'id'      => $identity['id'],
                'deleted' => 'N'
            ]
        ]);

        if ($user == false) {
            throw new \Exception('The user does not exist');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id'   => $user->id,
            'name' => $user->name,
            'role' => $user->role
        ]);
    }

    public function getUser()
    {
        $identity = $this->session->get('auth-identity');

        if (isset($identity['id'])) {
            $user = Users::findFirst([
                'conditions' => 'id = :id: AND deleted = :deleted:',
                'bind'       => [
                    'id'      => $identity['id'],
                    'deleted' => 'N'
                ]
            ]);

            if ($user == false) {
                $this->session->remove('auth-identity');
                throw new \Exception('The user does not exist', 401);
            }

            return $user;
        }

        return false;
    }
}
