<?php

namespace App\Acl;

use Phalcon\Acl\Adapter\Memory;

class Acl extends Memory
{
    protected $_defaultAccess = false;

    public function __construct()
    {
        parent::__construct();

        $this->addRole('guest');
        $this->addRole('user');
        $this->addRole('admin', 'user');

        $this->addResource('admin', '*');
        $this->addResource('user', '*');
        $this->addResource('App\Controllers\ErrorsController', '*');
        $this->addResource('App\Controllers\LoginController', ['index', 'logout']);

        $this->allow('guest', 'App\Controllers\LoginController', ['index']);
        $this->allow('guest', 'App\Controllers\ErrorsController', '*');
        $this->allow('user', 'App\Controllers\LoginController', ['logout']);
        $this->allow('user', 'App\Controllers\ErrorsController', '*');
        $this->allow('user', 'user', '*');
        $this->allow('admin', 'admin', '*');
    }
}
