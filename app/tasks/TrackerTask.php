<?php

use Carbon\Carbon;
use Phalcon\Cli\Task;

class TrackerTask extends Task
{
    public function stopAllAction($args = null)
    {
        $date = Carbon::yesterday();

        if (isset($args[0]))
            $date = Carbon::createFromFormat('Y-m-d', $args[0]);

        $date->setTimeFromTimeString('23:59:59');

        $this->modelsManager->executeQuery(
            'UPDATE App\Models\Tracker SET stop_time = :timestamp: WHERE DATE(start_time) = :date: AND stop_time IS NULL',
            [
                'date' => $date->format('Y-m-d'),
                'timestamp' => $date->format('Y-m-d H:i:s')
            ]
        );
    }
}
